<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class Note extends ActiveRecord {

	public static function tableName() {
		return '{{%note}}';
	}

	public function rules() {
		return [
			[['title'], 'string'],
		];
	}

	public function attributeLabels() {
		return [
			'title' => 'Имя',
			'desc' => 'Описание',
			'date' => 'Дата',
			'caegory_id' => 'Категория',
		];
	}

	public static function findIdentity($id) {
		return static::findOne(['id' => $id]);
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

	public function saveNote(){
		return $this->save();
	}

}
