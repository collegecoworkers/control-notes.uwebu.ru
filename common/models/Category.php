<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use common\models\Note;

class Category extends ActiveRecord {

	public static function tableName() {
		return '{{%category}}';
	}

	public function attributeLabels() {
		return [
			'title' => 'Название',
			'desc' => 'Описание',
			'date' => 'Дата создания',
		];
	}

	public function getCountNotes() {
		return Note::find()->where(['category_id' => $this->id])->count();
	}

	public static function findId($id) {
		return static::findOne(['id' => $id]);
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

}
