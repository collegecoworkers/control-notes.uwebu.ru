<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

use common\models\Note;
use common\models\Category;
use common\models\User;
use common\models\Login;
use common\models\SignUpForm;

class SiteController extends Controller
{

	public function behaviors() {
		return [
			'access' => ['class' => AccessControl::className(),
			'rules' => [
				[
					'actions' => [
						'signup',
						'login',
						'error'
					],
					'allow' => true,
				],
				[
					'actions' => [
						'logout',
						'index',
						'add-category',
						'update-category',
						'delete-category',
						'category',
						'down',
						'create',
						'update',
						'delete',
					],
					'allow' => true,
					'roles' => ['@'],
				],
			],
		],
	];
}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex() {
		if (Yii::$app->user->isGuest) return $this->goHome();

		$model = Category::find()->orderBy(['date' => SORT_ASC]);
		return $this->render('index', [
			'model' => $model
		]);
	}

	public function actionCategory($id) {
		if (Yii::$app->user->isGuest) return $this->goHome();
		$category = Category::find()->where(['id' => $id])->one();
		$model = Note::find()->where(['category_id' => $id]);
		return $this->render('category', [
			'category' => $category,
			'model' => $model,
		]);
	}

	public function actionCreate($id) {

		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Note();

		if (Yii::$app->request->isPost) {
			$model->title = Yii::$app->request->post()['Note']['title'];
			$model->desc = Yii::$app->request->post()['Note']['desc'];
			$model->category_id = $id;
			$model->date = date('Y-m-d');
			$model->save();
			return $this->redirect(['/site/category', 'id' => $id]);
		}

		return $this->render('create', [
			'model' => $model,
			'categorys' => $this->getCategorys(),
		]);
	}

	public function actionUpdate($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Note::findIdentity($id);

		if (Yii::$app->request->isPost) {
			$model->title = Yii::$app->request->post()['Note']['title'];
			$model->desc = Yii::$app->request->post()['Note']['desc'];
			$model->save();
			return $this->redirect(['/site/category', 'id' => $model->category_id]);
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	public function actionDelete($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Note::findIdentity($id);
		$model->delete();
		return $this->redirect(['index']);
	}

	public function actionAddCategory() {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Category();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->title = Yii::$app->request->post()['Category']['title'];
			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('add-category', [
			'model' => $model,
		]);
	}

	public function actionUpdateCategory($id) {
		if (Yii::$app->user->isGuest) return $this->goHome();

		$model = Category::findOne($id);

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->title = Yii::$app->request->post()['Category']['title'];
			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('add-category', [
			'model' => $model,
		]);
	}

	public function actionDeleteCategory($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Category::findOne($id);
		$model->delete();
		return $this->redirect(['index']);
	}

	public function actionSignup() {

		$this->layout = 'login';

		$model = new SignUpForm();

		if(isset($_POST['SignUpForm'])) {
			$model->attributes = Yii::$app->request->post('SignUpForm');
			if($model->validate() && $model->signup()) {
				return $this->redirect(['login']);
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}

	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		
		$this->layout = 'login';

		$model = new Login();

		if( Yii::$app->request->post('Login')) {
			$model->attributes = Yii::$app->request->post('Login');
			$user = $model->getUser();
			if($model->validate()) {
				if($user->isAdmin($user['id'])) {
					Yii::$app->user->login($user);
					return $this->goHome();
				}
			}
		}

		return $this->render('login', [
			'model' => $model,
		]);
	}

	public function actionLogout() {
		Yii::$app->user->logout();
		return $this->goHome();
	}

	public function getCategorys() {
		$categorys = [];
		$all_categorys = Category::find()->all();

		for ($i=0; $i < count($all_categorys); $i++) $categorys[$all_categorys[$i]->id] = $all_categorys[$i]->title;

		return $categorys;
	}

}
