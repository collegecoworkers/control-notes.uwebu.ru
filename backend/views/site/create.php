<?php

use yii\helpers\Html;
use backend\components\widgets\ActiveForm;

$this->title = Yii::t('app', 'Новая заметка');

?>
<div class="span9" id="content">
	<div class="panel panel-default">
		<div class="panel-body">

			<div class="link-update">
				<?php $form = ActiveForm::begin(); ?>

				<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
				<?= $form->field($model, 'desc')->textArea() ?>

				<div class="form-group center">
					<?= Html::submitButton(Yii::t('app', 'Добавить'), ['class' =>  'btn btn-success']) ?>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
