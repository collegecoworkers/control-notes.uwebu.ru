<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
	<?php $form = ActiveForm::begin(
		[
			'action' => '',
			'options' => [
				'class' => 'form-signin'
			]
		]
		); ?>

		<h2 class="form-signin-heading">Вход</h2>

		<?= $form->field($model, 'username')->textInput(['autofocus' => true])->input('text', ['placeholder'=>'Логин']) ?>
		<?= $form->field($model, 'email')->textInput()->input('text', ['placeholder'=>'email']) ?>
		<?= $form->field($model, 'password')->passwordInput()->input('password', ['placeholder'=>'Пароль'])?>

		<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
		<?= Html::a('Войти', ['/site/login'],['class' => 'btn']) ?>

		<?php ActiveForm::end(); ?>

</div> <!-- /container -->