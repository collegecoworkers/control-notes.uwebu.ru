<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
	<?php $form = ActiveForm::begin(
		[
			'action' => '',
			'options' => [
				'class' => 'form-signin'
			]
		]
		); ?>

		<h2 class="form-signin-heading">Вход</h2>

		<?= $form->field($model, 'email')->textInput(['autofocus' => true])->input('text', ['placeholder'=>'email']) ?>
		<?= $form->field($model, 'password')->passwordInput()->input('password', ['placeholder'=>'Password'])?>

		<?= Html::submitButton('Войти', ['class' => 'btn btn-large btn-primary']) ?>
		<?= Html::a('Регистрация', ['/site/signup'],['class' => 'btn']) ?>
		<?php ActiveForm::end(); ?>

</div> <!-- /container -->