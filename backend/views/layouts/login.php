<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use backend\assets\AdminLteAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AdminLteAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!-- Bootstrap -->
    <link href="/admin/public/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="/admin/public/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <link href="/admin/public/vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
    <link href="/admin/public/assets/styles.css" rel="stylesheet" media="screen">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="/admin/public/vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <?php $this->head() ?>
</head>
<body class="login-page" id="login">

    <?php $this->beginBody() ?>

    <?= $content ?>

    <script src="/admin/public/vendors/jquery-1.9.1.min.js"></script>
    <script src="/admin/public/bootstrap/js/bootstrap.min.js"></script>
    <script src="/admin/public/vendors/easypiechart/jquery.easy-pie-chart.js"></script>
    <script src="/admin/public/assets/scripts.js"></script>
    <script>
        $(function() {
            // Easy pie charts
            $('.chart').easyPieChart({animate: 1000});
        });
    </script>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
