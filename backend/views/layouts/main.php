<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use backend\assets\AppAsset;
use backend\assets\AdminLteAsset;

//AppAsset::register($this);
$asset      = AdminLteAsset::register($this);
$baseUrl    = $asset->baseUrl;
use backend\assets\FontAwesomeAsset;
FontAwesomeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- Bootstrap -->
    <link href="/admin/public/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="/admin/public/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <link href="/admin/public/vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
    <link href="/admin/public/assets/styles.css" rel="stylesheet" media="screen">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="/admin/public/vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <?php $this->beginBody() ?>

    <?= $this->render('header.php', ['baserUrl' => $baseUrl, 'title'=>Yii::$app->name]) ?>
  
<div class="container-fluid">
    <div class="row-fluid">
        <?= $this->render('leftside.php', ['baserUrl' => $baseUrl]) ?>
        <?= $this->render('content.php', ['content' => $content]) ?>
        <?= $this->render('footer.php', ['baserUrl' => $baseUrl]) ?>
        <?= $this->render('rightside.php', ['baserUrl' => $baseUrl]) ?>
    </div>
    <hr>
    <footer>
        <p>&copy; 2017 Заметки</p>
    </footer>
</div>

  <?php $this->endBody() ?>
  <script src="/admin/public/vendors/jquery-1.9.1.min.js"></script>
  <script src="/admin/public/bootstrap/js/bootstrap.min.js"></script>
  <script src="/admin/public/vendors/easypiechart/jquery.easy-pie-chart.js"></script>
  <script src="/admin/public/assets/scripts.js"></script>
  <script>
    $(function() {$('.chart').easyPieChart({animate: 1000});});
  </script>
</body>
</html>
<?php $this->endPage() ?>
