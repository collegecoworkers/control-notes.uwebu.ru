<?php
use yii\widgets\Breadcrumbs;
use yii\widgets\AlertLte;

?>
<div id="content">
	<div class="outer">
		<div class="inner bg-light lter">
			<div class="col-lg-12">

				<?php if (isset($this->blocks['content-header'])) { ?>
				<h1><?= $this->blocks['content-header'] ?></h1>
				<?php } else { ?>
				<h1>
					<?php
					if ($this->title !== null) {
						echo \yii\helpers\Html::encode($this->title);
					} else {
						echo \yii\helpers\Inflector::camel2words(
							\yii\helpers\Inflector::id2camel($this->context->module->id)
						);
						echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
					} ?>
				</h1>
				<?php } ?>

				<?=
				Breadcrumbs::widget([
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
					]) ?>
					<?= $content ?>
				</div>
			</div>
		</div>
	</div>