<?php

use adminlte\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\Category;

$category = [];
$all_category = Category::find()->all();

for ($i=0; $i < count($all_category); $i++) {
	$item = $all_category[$i];
	$category[] = [
		'label' => $item->title,
		'count' => $item->getCountNotes(),
		'url' => "/admin/site/category?id=$item->id",
		'active' => ($this->context->route == 'site/category' and $_GET['id'] == $item->id)
	];
}

?>

<div class="span3" id="sidebar">
	<ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
		<li class="<?= ($this->context->route == 'site/index') ? 'active' : '' ?>">
			<a href="/">Главная</a>
		</li>
		<?php foreach($category as $item): ?>
			<li class=" <?= (($item['active'] == 1) ? 'active' : '') ?>"><a href="<?= $item['url'] ?>"><span class="badge badge-info pull-right"><?= $item['count'] ?></span> <?= $item['label'] ?></a></li>
		<?php endforeach ?>
		<li class="<?= ($this->context->route == 'site/add-category') ? 'active' : '' ?>" ><a href="/admin/site/add-category"><i class="icon-plus"></i> Добавить категорию</a></li>
	</ul>
</div>