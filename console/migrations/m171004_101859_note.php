<?php

use yii\db\Migration;

class m171004_101859_note extends Migration
{
    public function safeUp()
    {
        $this->createTable('note', [
            'id' => $this->primaryKey(),
            'title' => $this->string(500),
            'desc' => $this->text(),
            'date' => $this->date(),
            'category_id' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%note}}');
    }
}
